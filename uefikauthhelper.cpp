// SPDX-FileCopyrightText: 2021 Marcello Massaro <spam@mmassa.ro>

// SPDX-License-Identifier: GPL-2.0-or-later
#include "uefikauthhelper.hpp"

#include <QProcess>

KAuth::ActionReply UEFIKAuthHelper::changebootopts(const QVariantMap& args)
{
    auto reply{KAuth::ActionReply()};
    QProcess proc;
    proc.setProgram(QLatin1String("efibootmgr"));
    if (args.contains(QLatin1String("bootNext"))) {
        auto bootNum{args[QLatin1String("bootNext")].toInt()};
        proc.setArguments({
            QLatin1String("-n"),
            QString(QLatin1String("%1"))
            .arg(bootNum, 8, 16, QLatin1Char('0'))
        });
        proc.start();
        proc.waitForFinished();
        reply.addData(QLatin1String("bootNextOutput"), QString::fromLatin1(proc.readAll()));
    }

    if (args.contains(QLatin1String("bootOrder"))) {
        auto bootOrder{args[QLatin1String("bootOrder")].toStringList()};
        proc.setArguments({
            QLatin1String("-o"),
            bootOrder.join(QLatin1Char(','))
        });
        proc.start();
        proc.waitForFinished();
        reply.addData(QLatin1String("bootOrderOutput"), QString::fromLatin1(proc.readAll()));
    }

    return reply;
}

KAUTH_HELPER_MAIN("org.kde.uefi", UEFIKAuthHelper)
