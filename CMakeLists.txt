# SPDX-FileCopyrightText: 2021 Marcello Massaro <spam@mmassa.ro>
#
# SPDX-License-Identifier: GPL-2.0-or-later

cmake_minimum_required(VERSION 3.0)

project(kcm_uefi)

set(QT_MIN_VERSION "5.14.0")
set(KF5_MIN_VERSION "5.73.0")

find_package(ECM ${KF5_MIN_VERSION} REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules)

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDEFrameworkCompilerSettings NO_POLICY_SCOPE)

find_package(Qt5 ${QT_MIN_VERSION} CONFIG REQUIRED COMPONENTS
    Quick
    Svg
)

find_package(KF5 ${KF5_MIN_VERSION} REQUIRED COMPONENTS
    Auth
    Config
    Declarative
    I18n
    KCMUtils
)

set(uefisettings_SRCS
    uefisettings.cpp
    uefilistmodel.cpp
    uefikauthhelper.cpp
)

add_library(kcm_uefi MODULE ${uefisettings_SRCS})
set_property(TARGET kcm_uefi PROPERTY CXX_STANDARD 17)
target_link_libraries(kcm_uefi
    KF5::Auth
    Qt5::Core
    KF5::CoreAddons
    KF5::I18n
    KF5::QuickAddons
)

add_executable(kcm_uefi_kauth_helper uefikauthhelper.cpp)
target_link_libraries(kcm_uefi_kauth_helper Qt5::Core KF5::Auth)

kcoreaddons_desktop_to_json(kcm_uefi "package/metadata.desktop")

install(TARGETS kcm_uefi DESTINATION ${KDE_INSTALL_PLUGINDIR}/kcms)
install(FILES package/metadata.desktop RENAME kcm_uefi.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR}) # Install the desktop file
install(TARGETS kcm_uefi_kauth_helper DESTINATION ${KAUTH_HELPER_INSTALL_DIR})

kpackage_install_package(package kcm_uefi kcms) # Install our QML kpackage.
kauth_install_actions(org.kde.uefi org.kde.uefi.actions)
kauth_install_helper_files(kcm_uefi_kauth_helper org.kde.uefi root)
