import QtQuick 2.12
import QtQuick.Controls 2.12

import org.kde.kirigami 2.12 as Kirigami
import org.kde.kcm 1.2 as KCM

KCM.ScrollViewKCM {
  header: Row {
    spacing: Kirigami.Units.smallSpacing
    Label { text: i18n("Select next boot"); height: parent.height }
    ComboBox {
      id: bootNextSelector
      model: kcm.model
      onCurrentIndexChanged: {
        kcm.model.setBootNext(currentValue)
        kcm.needsSave = true
      }
      textRole: "display"
      valueRole: "bootNumber"
    }
  }

  view: ListView {
    id: listView
    Kirigami.PlaceholderMessage {
      anchors.centerIn: parent
      width: parent.width - (Kirigami.Units.largeSpacing * 4)
      visible: listView.count === 0
      text: i18n("This system is not based on UEFI. You cannot change the boot order with this settings page.")
    }
    delegate: Component {
      Kirigami.SwipeListItem {
        Label {
          text: model.display + (model.isNext ? i18n(" (booting next)") : "")
        }

        Kirigami.Theme.colorSet: model.isNext ? Kirigami.Theme.Complementary : Kirigami.Theme.View

        actions: [
          Kirigami.Action {
            id: moveTop
            enabled: index
            iconName: "go-top"
            tooltip: i18n("Make boot option first")
            onTriggered: {
              kcm.model.moveTop(index)
              kcm.needsSave = true
            }
          },
          Kirigami.Action {
            id: moveUp
            enabled: index
            tooltip: i18n("Move boot option up")
            iconName: "go-up"
            onTriggered: {
              kcm.moveUp(index)
              kcm.needsSave = true
            }
          },
          Kirigami.Action {
            id: moveDown
            enabled: index !== (kcm.model.rowCount() - 1)
            tooltip: i18n("Move boot option down")
            iconName: "go-down"
            onTriggered: {
              kcm.moveDown(index)
              kcm.needsSave = true
            }
          }
        ]
      }
    }
    model: kcm.model
  }
  Binding {
    target: kcm
    property: "bootNext"
    value: bootNextSelector.currentValue
  }
}
