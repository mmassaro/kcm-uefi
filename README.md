# UEFI configuration for Plasma

This configuration module for the Plasma Settings application enables you to
change the boot order of your system and decide which of the various boot
options will be selected automatically on next boot.

## Installing from sources

As simple as

```sh
git clone https://invent.kde.org/mmassaro/kcm-uefi.git
mkdir build && cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr
make -j$(nproc) && sudo make install
```

It will then appear under the "Workspace" section, subsection "Startup and
shutdown".

## Dependencies

Besides of course having Plasma as you desktop environment, you need:

* `cmake`;
* Qt 5.14+;
* the ECM (`extra-cmake-modules`);
* KFrameworks 5.73+
    * KAuth
    * KConfig
    * KDeclarative
    * Kirigami2
* [`efibootmgr`](https://github.com/rhboot/efibootmgr), the CLI tool;
* a C++17 compliant compiler.