// SPDX-FileCopyrightText: 2021 Marcello Massaro <spam@mmassa.ro>

// SPDX-License-Identifier: GPL-2.0-or-later
#include "uefilistmodel.hpp"

#include <QDir>
#include <QProcess>
#include <QtGlobal>


UEFIListModel::UEFIListModel(QObject *parent) : QAbstractListModel(parent)
{
    if (supportsEfiVariables()) {
        proc = new QProcess{this};
        QObject::connect(proc, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), this,
                         qOverload<int, QProcess::ExitStatus>(&UEFIListModel::parseEfiBootMgrOutput));
        auto program{QString::fromUtf8("efibootmgr")};
        QStringList args;
        proc->start(program, args);
    } else {
        qDebug("EFI not supported");
    }
}

void UEFIListModel::parseEfiBootMgrOutput(const QStringList &stringList)
{
    beginResetModel();
    bootEntries.clear();
    bool bootNextIsSet{false};
    for (const auto &currentLine : stringList) {
        auto splitLine = currentLine.split(QLatin1Char(' '));
        auto fieldName = splitLine.first().remove(QLatin1Char(':'));
        splitLine.removeFirst();
        auto fieldValue = splitLine.join(QLatin1Char(' '));

        if (fieldName == QLatin1String("BootOrder")) {
            const auto bootOrderString = fieldValue.split(QLatin1Char(','));
            for (const auto &bo : bootOrderString) {
                bootOrder.append(bo.toInt(nullptr, 16));
            }
        } else if (fieldName == QLatin1String("BootNext")) {
            bootNextIsSet = true;
            bootNext = fieldValue.toInt(nullptr, 16);
        } else if (fieldName == QLatin1String("BootCurrent")) {
        } else if (fieldName.startsWith(QLatin1String("Boot"))) {
            auto bootNum{QStringRef(&fieldName, 4, 4).toInt(nullptr, 16)};
            bootEntries[bootNum] = {
                fieldValue, bootNum,
                fieldName.endsWith(QLatin1Char('*'))
            };
        }
    }
    if (!bootNextIsSet) {
        bootNext = -1;
    }
    endResetModel();
}

void UEFIListModel::parseEfiBootMgrOutput(
    int exitCode, QProcess::ExitStatus exitStatus)
{
    if (exitCode == EXIT_SUCCESS && exitStatus == QProcess::NormalExit) {
        parseEfiBootMgrOutput(QString::fromLatin1(proc->readAll().trimmed()).split(QLatin1Char('\n')));
    }
}

void UEFIListModel::moveUp(int row)
{
    if (row - 1 >= 0) {
        beginMoveRows(QModelIndex(), row, row, QModelIndex(), row - 1);
        bootOrder.move(row, row - 1);
        endMoveRows();
    }
}

void UEFIListModel::moveDown(int row)
{
    if (row + 1 < rowCount()) {
        // Why row+2? The simplest answer is "if I use row+1, it segfaults".
        //        ^-----------------------------------------------v
        beginMoveRows(QModelIndex(), row, row, QModelIndex(), row + 2);
        // I think the real reason is that something goes wrong because according
        // to the docs (https://doc.qt.io/qt-5/qabstractitemmodel.html#beginMoveRows)
        // the rows will be moved to the row BEFORE the target. Like, what? Why
        // would you make it like that? Anyway, in our case that means that the
        // rows are NOT moved at all, if we target row+1 as destination, because
        // then the row will be put BEFORE that, i.e. simply row, i.e. it will
        // not move. I don't know, it works like this...

        bootOrder.move(row, row + 1);
        endMoveRows();
    }
}


void UEFIListModel::moveTop(int row)
{
    beginMoveRows(QModelIndex(), row, row, QModelIndex(), 0);
    bootOrder.move(row, 0);
    endMoveRows();
}

void UEFIListModel::setBootNext(int bn)
{
    auto indexOld{createIndex(bootNext, 0)};
    auto indexNew{createIndex(bn, 0)};
    bootNext = bn;
    Q_EMIT dataChanged(indexOld, indexOld, {UEFIListModel::IsBootNextRole});
    Q_EMIT dataChanged(indexNew, indexNew, {UEFIListModel::IsBootNextRole});
}

void UEFIListModel::clearBootNext()
{
    auto index{createIndex(bootOrder.lastIndexOf(bootNext), 0)};
    bootNext = -1; // Any negative number will do
    Q_EMIT dataChanged(index, index, {UEFIListModel::IsBootNextRole});
}

bool UEFIListModel::supportsEfiVariables()
{
    QDir fi{QLatin1String("/sys/firmware/efi/efivars")};
    return fi.exists() && !fi.isEmpty();
}
QVariant UEFIListModel::data(const QModelIndex &index, int role) const
{
    if (index.parent().isValid()) {
        return QVariant();
    }

    auto bootNumber{bootOrder[index.row()]};

    switch (role) {
    case Qt::DisplayRole:
        return QVariant(bootEntries[bootNumber].description);
    case UEFIListModel::IsBootNextRole:
        return QVariant(bootNext == bootNumber);
    case UEFIListModel::NumberRole:
        return QVariant(bootNumber);
    default:
        return QVariant();
    }
}

int UEFIListModel::rowCount(const QModelIndex &parent) const
{
    return (parent.isValid() || !supportsEfiVariables()) ? 0 : bootOrder.count();
}

QHash<int, QByteArray> UEFIListModel::roleNames() const
{
    return {
        {UEFIListModel::ActiveRole, "active"},
        {UEFIListModel::NumberRole, "bootNumber"},
        {UEFIListModel::IsBootNextRole, "isNext"},
        {Qt::DisplayRole, "display"}
    };
}
