// SPDX-FileCopyrightText: 2021 Marcello Massaro <spam@mmassa.ro>

// SPDX-License-Identifier: GPL-2.0-or-later
#pragma once

#include <QAbstractListModel>
#include <QList>
#include <QProcess>

struct BootEntry {
    QString description;
    int number;
    bool active;
};

/**
 * The Boot Order list currently defined in the NVRAM.
 */
class UEFIListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {NumberRole = Qt::UserRole, ActiveRole, IsBootNextRole};

public:
    /**
     * Default constructor
     */
    UEFIListModel(QObject *parent = nullptr);

    /**
     * Destructor
     */
    ~UEFIListModel() override = default;

    static bool supportsEfiVariables();
    [[nodiscard]] QVariant data(const QModelIndex &index = QModelIndex(), int role = Qt::DisplayRole) const override;
    [[nodiscard]] int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    [[nodiscard]] QHash<int, QByteArray> roleNames() const override;

public Q_SLOTS:
    void parseEfiBootMgrOutput(const QStringList&);
    void parseEfiBootMgrOutput(int, QProcess::ExitStatus);
    void moveUp(int);
    void moveDown(int);
    void moveTop(int);
    void setBootNext(int);
    void clearBootNext();

private:
    QProcess *proc;
    QList<int> bootOrder;
    int bootNext{-1};
    // The mapping is a numeric boot number,
    QMap<int, BootEntry> bootEntries;
};
