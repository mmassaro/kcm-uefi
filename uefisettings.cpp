// SPDX-FileCopyrightText: 2021 Marcello Massaro <spam@mmassa.ro>

// SPDX-License-Identifier: GPL-2.0-or-later
#include "uefisettings.hpp"
#include "uefilistmodel.hpp"

#include <KPluginFactory>
#include <KLocalizedString>
#include <KAboutData>
#include <KAuth>
#include <KAuthAction>

K_PLUGIN_CLASS_WITH_JSON(UEFISettings, "metadata.json")

UEFISettings::UEFISettings(QObject *parent, const QVariantList &args) : KQuickAddons::ConfigModule(parent, args), model_(new UEFIListModel(this)) {
    auto *aboutData = new KAboutData(
        QStringLiteral("kcm_uefi"),
        i18nc("@title", "UEFI Boot Order"),
        QStringLiteral("0.1"),
        QStringLiteral(""),
        KAboutLicense::LicenseKey::GPL_V2,
        i18nc("@info:credit", "Copyright 2021 Marcello Massaro")
    );

    aboutData->addAuthor(
        i18nc("@info:credit", "Marcello Massaro"),
        QStringLiteral("spam@mmassa.ro")
    );

    setAboutData(aboutData);
    setRootOnlyMessage(i18n("Changing boot setting requires administrator (root) priviliges!"));
    setAuthActionName(QLatin1String("org.kde.uefi.changebootopts"));
//    setAuthActionName(QLatin1String("org.kde.uefi.changebootopts"));
    setButtons(Apply | Help);
    QObject::connect(model(), &UEFIListModel::dataChanged, this, [this](){setNeedsSave(true);setNeedsAuthorization(true);});
}

UEFIListModel *UEFISettings::model() const noexcept { return model_; }
void UEFISettings::setModel(UEFIListModel *model) {
  model_ = model;
  model_->setParent(this);
  Q_EMIT modelChanged(model_);
}

void UEFISettings::save()
{
  QVariantMap args;
  args[QLatin1String("bootNext")] = 0;
  KAuth::Action changeBootOpts(QLatin1String("org.kde.uefi.changebootopts"));
  changeBootOpts.setHelperId(QLatin1String("org.kde.uefi"));
  changeBootOpts.setArguments(args);
  auto job{changeBootOpts.execute()};
  if(job->exec())
    setNeedsSave(false);
}

int UEFISettings::bootNext() const noexcept
{
  return bootNext_;
}

void UEFISettings::setBootNext(int value) {
  bootNext_ = value;
  Q_EMIT bootNextChanged(bootNext_);
}

#include "uefisettings.moc"
