// SPDX-FileCopyrightText: 2021 Marcello Massaro <spam@mmassa.ro>

// SPDX-License-Identifier: GPL-2.0-or-later
#pragma once

#include <KAuth>

class UEFIKAuthHelper : public QObject
{
    Q_OBJECT
public:
    UEFIKAuthHelper() = default;
    ~UEFIKAuthHelper() = default;
public Q_SLOTS:
    KAuth::ActionReply changebootopts(const QVariantMap& args);
};

