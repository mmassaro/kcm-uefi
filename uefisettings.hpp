// SPDX-FileCopyrightText: 2021 Marcello Massaro <spam@mmassa.ro>

// SPDX-License-Identifier: GPL-2.0-or-later
#pragma once

#include "uefilistmodel.hpp"

#include <QObject>
#include <KQuickAddons/ConfigModule>

class UEFISettings : public KQuickAddons::ConfigModule
{
    Q_OBJECT
    Q_PROPERTY(UEFIListModel* model READ model WRITE setModel NOTIFY modelChanged)
    Q_PROPERTY(int bootNext READ bootNext WRITE setBootNext NOTIFY bootNextChanged)
public:
    UEFISettings(QObject *parent, const QVariantList &args);
    ~UEFISettings() override = default;

    [[nodiscard]] UEFIListModel* model() const noexcept;
    void setModel(UEFIListModel* model);

    void save() override;

    [[nodiscard]] int bootNext() const noexcept;
    void setBootNext(int value);

Q_SIGNALS:
    void modelChanged(UEFIListModel* model);
    void bootNextChanged(int value);

private:
    UEFIListModel* model_;
    int bootNext_;
};
